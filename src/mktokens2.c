
#include <stdio.h>
#include <string.h>

typedef struct {
    const char *cons;
    int endlist;
    const char *name;
    const char *str_name;
} tok;

#define STRINGIFY(x) #x
#define ENTRY(x, y, z) { #x, y, #z, STRINGIFY(#z) },

const tok toks[] = {
ENTRY(TEOF,	1,	end of file)
ENTRY(TNL,	0,	newline)
ENTRY(TSEMI,	0,	";")
ENTRY(TBACKGND, 0,	"&")
ENTRY(TAND,	0,	"&&")
ENTRY(TOR,	0,	"||")
ENTRY(TPIPE,	0,	"|")
ENTRY(TLP,	0,	"(")
ENTRY(TRP,	1,	")")
ENTRY(TENDCASE, 1,	";;")
ENTRY(TENDBQUOTE, 1,	"`")
ENTRY(TREDIR,	0,	redirection)
ENTRY(TWORD,	0,	word)
ENTRY(TNOT,	0,	"!")
ENTRY(TCASE,	0,	"case")
ENTRY(TDO,	1,	"do")
ENTRY(TDONE,	1,	"done")
ENTRY(TELIF,	1,	"elif")
ENTRY(TELSE,	1,	"else")
ENTRY(TESAC,	1,	"esac")
ENTRY(TFI,	1,	"fi")
ENTRY(TFOR,	0,	"for")
ENTRY(TIF,	0,	"if")
ENTRY(TIN,	0,	"in")
ENTRY(TTHEN,	1,	"then")
ENTRY(TUNTIL,	0,	"until")
ENTRY(TWHILE,	0,	"while")
ENTRY(TBEGIN,	0,	"{")
ENTRY(TEND,	1,	"}")
};

int main() {
    // Create token.h
    FILE *f = fopen("token.h", "w");
    if (!f) {
        perror("Cannot open token.h");
    }
    for (int i = 0; i < sizeof(toks) / sizeof(toks[0]); i++) {
        fprintf(f, "#define %s %d\n", toks[i].cons, i);
    }
    fclose(f);

    // Create token_vars.h
    f = fopen("token_vars.h", "w");
    if (!f) {
        perror("Cannot open token_vars.h");
    }
    fprintf(f, "\n/* Array indicating which tokens mark the end of a list */\n");
    fprintf(f, "static const char tokendlist[] = {\n");
    for (int i = 0; i < sizeof(toks) / sizeof(toks[0]); i++) {
        fprintf(f, "\t%d,\n", toks[i].endlist);
    }
    fprintf(f, "};\n\n");
    fprintf(f, "static const char *const tokname[] = {\n");
    for (int i = 0; i < sizeof(toks) / sizeof(toks[0]); i++) {
        fprintf(f, "\t%s,\n", toks[i].str_name);
    }
    fprintf(f, "};\n\n");
    int not_idx;
    for (not_idx = 0; not_idx < sizeof(toks) / sizeof(toks[0]); not_idx++) {
        if (strcmp(toks[not_idx].cons, "TNOT") == 0) {
            break;
        }
    }
    fprintf(f, "#define KWDOFFSET %d\n\n", not_idx);
    fprintf(f, "static const char *const parsekwd[] = {\n");
    for (int i = not_idx; i < sizeof(toks) / sizeof(toks[0]); i++) {
        fprintf(f, "\t%s,\n", toks[i].name);
    }
    fprintf(f, "};\n\n");
    fclose(f);

    return 0;
}
