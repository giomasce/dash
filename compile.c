
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <libgen.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

void my_system(const char *cmd) {
    char *args[256];
    char cmd2[1024];
    strcpy(cmd2, cmd);
    fprintf(stderr, "%s\n", cmd);
    int argc = 0;
    // Simple command parsing inspired by https://github.com/maxbla/minimal-shell/blob/master/msh.c (GPL-2+)
    args[argc] = strtok(cmd2, " ");
    while (argc < sizeof(args) / sizeof(args[0]) && (args[++argc] = strtok(NULL, " ")) != NULL);
    pid_t pid = fork();
    if (pid == 0) {
        execvp(args[0], args);
        _exit(-1);
    }
    int status;
    wait(&status);
    if (!(WIFEXITED(status) && WEXITSTATUS(status) == 0)) {
        fprintf(stderr, "Command failed: %s\n", cmd);
        exit(-1);
    }
}

const char *HELPERS[] = {
    "mknodes",
    "mktokens2",
    "mkinit",
    "mksignames",
    "mkbuiltins2",
};

const char *FILES[] = {
    "alias",
    "arith_yacc",
    "arith_yylex",
    "cd",
    "error",
    "eval",
    "exec",
    "expand",
    "histedit",
    "input",
    "jobs",
    "mail",
    "main",
    "memalloc",
    "miscbltin",
    "mystring",
    "options",
    "parser",
    "redir",
    "show",
    "trap",
    "output",
    "bltin/printf",
    "system",
    "bltin/test",
    "bltin/times",
    "var",
};

const char *FILES_GEN[] = {
    "builtins",
    "init",
    "nodes",
    "signames",
    "syntax",
};

int main(int argc, char *argv[]) {
    char buf[1024], buf2[1024];

    if (argc != 2) {
        fprintf(stderr, "Usage: %s CC\n", argv[0]);
        return 1;
    }
    const char *CC = argv[1];

    // Chdir to the src directory
    sprintf(buf, "%s/src", dirname(argv[0]));
    if (chdir(buf)) {
        perror("Cannot chdir");
        return 1;
    }

    // Create config.h
    FILE *fconf = fopen("../config.h", "w");
    fprintf(fconf,
            "#define HAVE_DECL_ISBLANK 1\n"
            "#define HAVE_ISALPHA 1\n"
            "#define HAVE_PATHS_H 1\n"
            "#define HAVE_STRTOD 1\n"
            "#define HAVE_KILLPG 1\n"
            "#define HAVE_STRSIGNAL 1\n"
            "#define HAVE_BSEARCH 1\n"
            "#define HAVE_SYSCONF 1\n"
            "#define SMALL 1\n"
            "#ifndef _GNU_SOURCE\n"
            "# define _GNU_SOURCE 1\n"
            "#endif\n");
    fclose(fconf);

    // Compile helpers
    sprintf(buf, "%s -DHAVE_CONFIG_H -I. -I..  -include ../config.h -DBSD=1 -DSHELL  -Wall -g -O2 -E -x c -o builtins.def builtins.def.in", CC);
    my_system(buf);
    for (int i = 0; i < sizeof(HELPERS) / sizeof(HELPERS[0]); i++) {
        sprintf(buf, "%s -I. -I.. -DBSD=1 -DSHELL -g -O2 -o %s %s.c", CC, HELPERS[i], HELPERS[i]);
        my_system(buf);
    }

    // Run helpers
    my_system("./mknodes nodetypes nodes.c.pat");
    my_system("./mktokens2");
    sprintf(buf, "./mkinit");
    for (int i = 0; i < sizeof(FILES) / sizeof(FILES[0]); i++) {
        sprintf(buf2, "%s %s.c", buf, FILES[i]);
        strcpy(buf, buf2);
    }
    my_system(buf);
    my_system("./mkbuiltins2");
    my_system("./mksignames");

    // Compile and run mksyntax
    sprintf(buf, "%s -I. -I.. -DBSD=1 -DSHELL -g -O2 -o %s %s.c", CC, "mksyntax", "mksyntax");
    my_system(buf);
    my_system("./mksyntax");

    // Compile all files
    for (int i = 0; i < sizeof(FILES) / sizeof(FILES[0]); i++) {
        sprintf(buf, "%s -DHAVE_CONFIG_H -I. -I.. -include ../config.h -DBSD=1 -DSHELL -Wall -g -c -o %s.o %s.c", CC, FILES[i], FILES[i]);
        my_system(buf);
    }
    for (int i = 0; i < sizeof(FILES_GEN) / sizeof(FILES_GEN[0]); i++) {
        sprintf(buf, "%s -DHAVE_CONFIG_H -I. -I.. -include ../config.h -DBSD=1 -DSHELL -Wall -g -c -o %s.o %s.c", CC, FILES_GEN[i], FILES_GEN[i]);
        my_system(buf);
    }

    // Link everything
    sprintf(buf, "%s -Wall -g -o dash", CC);
    for (int i = 0; i < sizeof(FILES) / sizeof(FILES[0]); i++) {
        sprintf(buf2, "%s %s.o", buf, FILES[i]);
        strcpy(buf, buf2);
    }
    for (int i = 0; i < sizeof(FILES_GEN) / sizeof(FILES_GEN[0]); i++) {
        sprintf(buf2, "%s %s.o", buf, FILES_GEN[i]);
        strcpy(buf, buf2);
    }
    my_system(buf);

    return 0;
}
